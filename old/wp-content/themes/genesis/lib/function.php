<?php
// Start the engine the other way
 
 //* Load the Genesis core files
include_once( get_template_directory() . '/lib/init.php' );
//* Child theme Definitions
define( 'CHILD_THEME_NAME', __( 'genesischild', 'genesischild' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/genesis/' );
define( 'CHILD_THEME_VERSION', '2.1.0' );
add_theme_support( 'html5' );
add_theme_support( 'genesis-responsive-viewport' );
//* Add HTML5 markup structure
add_theme_support( 'html5' );
//* Add HTML5 markup structure
add_theme_support( 'genesis-responsive-viewport' );