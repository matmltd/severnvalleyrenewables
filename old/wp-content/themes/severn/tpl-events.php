<?php 
/*
Template Name: Events
*/ 
// Add Read More Link to Excerpts
add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
	return '...&nbsp;<a href="' . get_permalink() . '"><br><br>Read&nbsp;More</a>';
}

?>

<?php 

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'ek_custom_loop' );
function ek_custom_loop() { ?>
<div class="content">
	<div class="entry">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="entry-content">
				<?php the_content(); ?>

			<?php endwhile; else: endif; ?>
			<!-- display future posts! -->
			<?php query_posts('category_name='.get_the_title().'&post_status=publish,future');?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php
 				// Assign the month to a variable
				$month = the_date('F, Y', '', '', FALSE);


 				// If your month hasn't been echoed earlier in the loop, echo it now
				if ($month !== $month_check) {
					echo "<h3 class='months'>" . $month .  "</h3>";

				}
				// Now that your month has been printed, assign it to the $month_check variable
				$month_check = $month;
			?>
			<ul>
			<li>
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<?php the_meta(); ?>
				<p><?php the_excerpt(); ?> </p>
			</li>
		</ul>

				<?php endwhile; else: endif; ?>	
		</div>
	</div>
</div>
<?php } genesis(); ?>