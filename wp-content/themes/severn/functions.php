<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'enterprise', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'enterprise' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Enterprise Pro Theme', 'enterprise' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/enterprise/' );
define( 'CHILD_THEME_VERSION', '2.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Enqueue Scripts
add_action( 'wp_enqueue_scripts', 'enterprise_load_scripts' );
function enterprise_load_scripts() {

	wp_enqueue_script( 'enterprise-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );
	
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:300,700,300italic|Titillium+Web:600', array(), CHILD_THEME_VERSION );
	
}

//* Add new image sizes
add_image_size( 'featured-image', 358, 200, TRUE );
add_image_size( 'home-top', 750, 600, TRUE );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'height'          => 129,
	'width'           => 400,
) );

//* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'enterprise-pro-black'	=> __( 'Enterprise Pro Black', 'enterprise' ),

) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 7 );

//* Reduce the secondary navigation menu to one level depth
add_filter( 'wp_nav_menu_args', 'enterprise_secondary_menu_args' );
function enterprise_secondary_menu_args( $args ){

	if( 'secondary' != $args['theme_location'] )
	return $args;

	$args['depth'] = 1;
	return $args;

}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'enterprise_remove_comment_form_allowed_tags' );
function enterprise_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Relocate after entry widget
remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );
add_action( 'genesis_after_entry', 'genesis_after_entry_widget_area', 5 );

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'enterprise' ),
	'description' => __( 'This is the top section of the homepage.', 'enterprise' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'enterprise' ),
	'description' => __( 'This is the bottom section of the homepage.', 'enterprise' ),
) );
// Show Single Future Posts
add_filter('the_posts', 'show_future_posts');
function show_future_posts($posts){
   global $wp_query, $wpdb;
   if(is_single() && $wp_query->post_count ==0){
      $posts = $wpdb->get_results($wp_query->request);
   }
   return $posts;
};
add_filter( 'post_link', 'future_permalink', 10, 3 );
//Fix future permalinks
function future_permalink( $permalink, $post, $leavename ) {
	/* for filter recursion (infinite loop) */
	static $recursing = false;

	if ( empty( $post->ID ) ) {
		return $permalink;
	}

	if ( !$recursing ) {
		if ( isset( $post->post_status ) && ( 'future' === $post->post_status ) ) {
			// set the post status to publish to get the 'publish' permalink
			$post->post_status = 'publish';
			$recursing = true;
			return get_permalink( $post, $leavename ) ;
		}
	}

	$recursing = false;
	return $permalink;
}

// toggle FAQ
function dtwd_scripts_enqueue() {
    wp_enqueue_script(
        'faq-toggle', get_stylesheet_directory_uri() . '/js/faq-toggle.js', array('jquery')
    );
}
add_action( 'wp_enqueue_scripts', 'dtwd_scripts_enqueue' );

     
 function faq_toggle( $atts, $content = null ) {
 extract( shortcode_atts(
 array(
 'title' => 'Click To Open',
 'color' => 'dark'
 ),
 $atts ) );
 return '<h3 class="trigger toggle-'.$color.'"><a href="#">'. $title .'</a></h3><div class="toggle_container">' . do_shortcode($content) . '</div>';
 }
 add_shortcode('faq-toggle', 'faq_toggle');
