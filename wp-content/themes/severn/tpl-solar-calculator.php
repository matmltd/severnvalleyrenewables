<?php 
/*
Template Name: Solar Calculator
*/ 
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
add_action( 'genesis_entry_content', 'svr_custom_loop' );
function svr_custom_loop() {
?>
<!-- BEGIN SOLAR GUIDE CALCULATOR WIDGET -->
<div id='solarguidewidget' style='background-image: url("http://www.solarguide.co.uk/images/iframe-preload-anim.gif"); min-width:100px; min-height:150px; background-repeat: no-repeat; background-position: 329px 120px;'>
<div id='solarguidecalculator'>&nbsp;</div>
<div id='solarguidecalculatorcredit'><p style='font-size: 10px;'>Solar PV calculator provided courtesy of <a rel='nofollow' href='http://www.solarguide.co.uk/'>Solar Guide</a>.</p></div>
<script type='text/javascript' src='http://www.solarguide.co.uk/js/includesolarcalc.php?user=c3a0a5d5-2c99-11e5-a123-0649c6da3a34&amp;width=658'></script>
<!-- General Notes:
Please do not change any part of this widget - doing so may prevent it from working properly and of course invalidate your free license.
Putting this widget on another site? Just get another free license here: http://www.solarguide.co.uk/solar-calculator-yoursite
-->
</div>
<!-- END SOLAR GUIDE CALCULATOR WIDGET -->

</div>


<?php
}
genesis();